# Instructions
1. **In the letter_of_attendence.csv add `name;from_date;to_date`**  
**one line per entry**  
**Example:**  

        Roar Emaus;4;14  
		Violetta Sagun;14;24

2. **Run `python attendence.py`**
3. **Run `make`**
4. **Now all pdfs are in the `output` folder**

`make clean` removes all temporary files

`make cleanall` removes all files in output folder
