import csv

def att_write():
    template = 'template/letter_of_attendance.tex' 
    with open(template,'r') as h:
        document = h.read()
    f   = open('letter_of_atten.csv','r')
    input= csv.reader(f,delimiter=';',quotechar='"') 
    next(input) 
    for row in input:
        string = row[0].replace(' ','_')+'_'+row[1].replace(' ','_')+'.tex'
        if not row[2] or not row[3]:
            row[2] = '\\hspace{2cm} '
            row[3] = ''
        else:
            row[2] = row[2]+' - '+row[3]

        custom_string = 'This is to certify that '+\
            '{{\\color{{blue}}\\bf {} {}}} '.format(row[1], row[0])+\
            'has participated in the 5 th International '+\
            'Conference on New Frontiers in Physics'+\
            '(ICNFP2016), held at the Ortodox Academy of Crete'+\
            ', Kolymbari, Crete, Greece, July, {}, 2016.\n'\
            .format(row[2])
        outdoc = document.replace('%INSERT_HERE',custom_string)
        with open('output/'+string,'w') as g:
            g.write(outdoc)

if __name__=='__main__':
    att_write()
