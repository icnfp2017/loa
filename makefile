TEX = latexmk -pdf --output-directory=output -bibtex -file-line-error -interaction=nonstopmode -use-make

.PHONY: main all clean 

all : 
	$(TEX) output/*.tex

clean :
	rm output/*.aux output/*.fls output/*.log output/*.fdb*

cleanall :
	rm -r output
	mkdir output

